/* eslint-disable camelcase */
/** Copyright (c) Blue Duck Education Ltd. (https://www.mangahigh.com), All rights reserved */
import React from 'react';
import TextBlock from 'prodigi-plugin-text-block';
import PropTypes from 'prop-types';
// import DemoPlugin from 'prodigi-helper-forms-preview/demoPlugin';

class Test_Translations extends React.Component {
  static propTypes = {
    _: PropTypes.func.isRequired,
  }

  render() {
    const {
      _,
    } = this.props;

    const sub1 = '--Substitution 1--';
    const count = 2;

    return (
      <div>
        <TextBlock text={_('I18N TEST: Single text.')} />
        <TextBlock text={_('I18N TEST: Single text.')} />
        <TextBlock text={_('I18N TEST: Single text.')} />

        <TextBlock text={_('I18N TEST: Single text, with comma.')} />

        <TextBlock text={_('I18N TEST: Single text with escapes \'.')} />
        <TextBlock text={_('I18N TEST: Single text with "quotes".')} />
        <TextBlock text={_('I18N TEST: Single text, with (brackets).')} />
        <TextBlock text={_('I18N TEST: Single text, with single "(" bracket.')} />

        <TextBlock text={_('I18N TEST: Single text - short hand substitution [[sub1]].', { sub1 })} />
        <TextBlock text={_('I18N TEST: Single text - long hand substitution [[sub]].', { sub: sub1 })} />
        <TextBlock text={_('I18N TEST: Single text - inline substitution [[sub]].', { sub: '--Substitution--' })} />
        <TextBlock text={_('I18N TEST: Single text - multiple substitution types A [[sub]] [[sub1]].', { sub: '--Substitution--', sub1 })} />
        <TextBlock text={_('I18N TEST: Single text - multiple substitution types B [[sub]] [[sub1]].', { sub1, sub: '--Substitution--' })} />
        <TextBlock text={_('I18N TEST: Single text - multiple inline substitution [[sub]] [[sub1]].', { sub: '--Substitution--', sub1: '--Substitution1--' })} />

        <TextBlock text={_(
            'I18N TEST: Plural text One - single substitution [[sub]].',
            'I18N TEST: Plural text Many - single substitution [[sub]].',
            count,
            { sub: sub1 },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - single substitution [[sub]].',
            'I18N TEST: Plural text Many - single substitution [[sub]].',
            count,
            { sub: sub1 },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - single substitution [[sub]].',
            'I18N TEST: Plural text Many - single substitution alt [[sub]].',
            count,
            { sub: sub1 },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - multiple substitution types A [[sub]] [[sub1]].',
            'I18N TEST: Plural text Many - multiple substitution types A [[sub]] [[sub1]].',
            count,
            { sub: '--Substitution--', sub1 },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - multiple substitution types B [[sub]] [[sub1]].',
            'I18N TEST: Plural text Many - multiple substitution types B [[sub]] [[sub1]].',
            count,
            { sub1, sub: '--Substitution--' },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - multiple substitution types C [[sub]] [[sub1]].',
            'I18N TEST: Plural text Many - multiple substitution types C [[sub]] [[sub1]].',
            count,
            {
              sub: '--Substitution--',
              sub1,
            },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - multiple substitution types D [[sub]] [[sub1]].',
            'I18N TEST: Plural text Many - multiple substitution types D [[sub]] [[sub1]].',
            count,
            {
              sub1,
              sub: '--Substitution--',
            },
          )}
        />

        <TextBlock text={_(
            'I18N TEST: Plural text One - multiple inline substitution [[sub]] [[sub1]].',
            'I18N TEST: Plural text Many - multiple inline substitution [[sub]] [[sub1]].',
            count,
            { sub: '--Substitution--', sub1: '--Substitution1--' },
          )}
        />
      </div>
    );
  }

export default Test_Translations;
