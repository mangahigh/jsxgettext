#!/usr/bin/env node
const fs = require('fs');
const shell = require('shelljs');
const acorn = require('acorn');
const jsx = require('acorn-jsx');

const args = require('minimist')(process.argv.slice(2));

/**
 * Show help.
 * @param {string} [message] Optional error message to show with help
 */
const _showHelp = function (message) {
  if (message) {
    console.log(message + "\n");
  }

  console.log('jsxgettext: A xgettext scanner for JSX');

  if (message) {
      process.exit(1);
  } else {
      process.exit(0);
  }
}

if (!args['o']) {
  _showHelp("Output file argument is required");
}

if (!args['_'] || args['_'].length === 0) {
  _showHelp("Must provide at least one input file");
}


const outFile = args['o'];
const inputFiles = args['_'];

console.log(`Scanning to ${outFile}`);

let lines = [];
const out = [];

inputFiles.forEach((fileName) => {
  lines = fs.readFileSync(fileName, 'utf-8')
    .split(/(?=\b[_t]\(.*)|\n/);

  let lineNumber = 0;

  let currentMatch;
  let openCount = 0;
  const items = {};
  let section;
  let targetLine;

  openCount = 0;
  currentMatch = '';
  lines.forEach((line) => {
    lineNumber += 1;
    // console.log(lineNumber, line);

    if (line.match(/\b[_t]\(/) || openCount) {
      if (openCount) {
        section = line;
      } else {
        section = line.substring(line.match(/\b[_t]\(.*/).index);
      }

      item = '';
      do {
        section.split('').some((char) => {
          textParts = section.match(/^(['])((\\{2})*|(.*?[^\\](\\{2})*))\1/);

          if (textParts) {
            currentMatch += textParts[0];
            targetLine = lineNumber;
            section = section.substring(textParts[0].length);
            return true;
          }

          section = section.substr(1);

          if (char === '(') {
            if (openCount === 0) {
              targetLine = lineNumber;
            }

            openCount += 1;
          } else if (char === ')') {
            openCount -= 1;
          }

          currentMatch += char;

          if (currentMatch.length > 1 && openCount === 0) {
            item = currentMatch;
            currentMatch = '';
            openCount = 0;
            return true;
          }

          return false;
        });
      } while (!item && section);

      items[`${fileName}:${targetLine}`] = item;
    }
  });

  let result;
  let arguments;

  Object.keys(items).forEach((key) => {
    result = acorn.Parser.extend(jsx()).parse(items[key]);
    if (result.body && result.body[0]) {
        arguments = result.body[0].expression.arguments;
        if (arguments[0] && arguments[0].value) {
          out.push(`#: ${key}`);
          out.push(`msgid "${arguments[0].value.replace(/"/g, '\\"')}"`);

          if (arguments[1] && arguments[1].type === 'Literal') {
            const value = typeof arguments[1].value !== 'string' ? `${arguments[1].value}` : arguments[1].value;

            out.push(`msgid_plural "${value.replace(/"/g, '\\"')}"`);
            out.push('msgstr[0] ""');
            out.push('msgstr[1] ""');
          } else {
            out.push('msgstr ""');
          }

          out.push('\n');
        }
    }
  });
});


const header = `# Prodigi Template Translation
#
msgid ""
msgstr ""
"Project-Id-Version: Prodigi\\n"
"POT-Creation-Date: 2008-02-06 16:25-0500\\n"
"PO-Revision-Date: 2008-02-09 15:23+0200\\n"
"Last-Translator: \\n"
"Language-Team: Mangahigh <sysop@mangahigh.com>\\n"
"MIME-Version: 1.0\\n"
"Content-Type: text/plain; charset=UTF-8\\n"
"Content-Transfer-Encoding: 8bit\\n"
`;

fs.writeFile(outFile, `${header}\n${out.join('\n')}`, (err) => {
  if (err) {
    return console.log(err);
  }

  shell.exec(`msguniq -i ${outFile} -o ${outFile}`);
  console.log('Complete');

  return true;
});
